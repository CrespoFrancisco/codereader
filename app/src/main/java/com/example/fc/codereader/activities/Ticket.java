package com.example.fc.codereader.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.fc.codereader.R;

import org.w3c.dom.Text;

public class Ticket extends AppCompatActivity implements View.OnClickListener{
    Button abonar;
    TextView ticket;
    TextView numero_tarjeta,monto,horario,vencimiento;

    private final String TARJETA ="****-****-****-";
    private final String VENCIMIENTO="**";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);

        abonar = (Button)findViewById(R.id.ticket_abonar);
        ticket = (TextView)findViewById(R.id.ticket_numero);
        numero_tarjeta =(TextView)findViewById(R.id.ticket_num_tarjeta);
        monto = (TextView)findViewById(R.id.ticket_monto);
        horario=(TextView)findViewById(R.id.ticket_horario);
        vencimiento=(TextView)findViewById(R.id.ticket_vencimiento);
        Bundle b = new Bundle();
        b = getIntent().getExtras();

        String nTarjeta = b.getString("NumTarjeta");


        ticket.setText(b.getString("Barcode"));
        numero_tarjeta.setText(TARJETA+b.getString("NumTarjeta").substring(15,19));
        vencimiento.setText(VENCIMIENTO+b.getString("FechaVencimiento").substring(2,5));
        monto.setText(b.getString("Monto"));
        horario.setText(b.getString("Horario"));



        abonar.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

    }
}
