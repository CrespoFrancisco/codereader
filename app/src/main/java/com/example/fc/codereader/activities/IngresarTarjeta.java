package com.example.fc.codereader.activities;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.fc.codereader.R;

public class IngresarTarjeta extends AppCompatActivity implements View.OnClickListener,TextWatcher,View.OnKeyListener{


    private static final char minus='-';
    private static final char barra = '/';

    boolean fVencimiento=false,fnumero=true,fcvc=false;


    EditText numero,vencimiento,cvc;
    TextView ticket, monto,horario;


    Button guardarTarjeta;
    Bundle b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tarjeta);

        b = new Bundle();
        b = getIntent().getExtras();

        setViews();


    }


    private void setViews() {
        numero= (EditText)findViewById(R.id.ingresar_numero);
        vencimiento = (EditText)findViewById(R.id.ingresar_mmyy);
        cvc = (EditText)findViewById(R.id.ingresar_cvc);
        ticket =(TextView)findViewById(R.id.tarjeta_ticket);
        guardarTarjeta = (Button)findViewById(R.id.btn_guardar_tarjeta);
        monto =(TextView)findViewById(R.id.tarjeta_monto);
        horario =(TextView)findViewById(R.id.tarjeta_horario);


      //  image.setClipToOutline(true);


        ticket.setText(b.getString("Barcode").toString());

        numero.setOnClickListener(this);
        vencimiento.setOnClickListener(this);
        cvc.setOnClickListener(this);

        numero.addTextChangedListener(this);
        vencimiento.addTextChangedListener(this);
        cvc.addTextChangedListener(this);

        numero.setOnKeyListener(this);
        vencimiento.setOnKeyListener(this);
        cvc.setOnKeyListener(this);



        numero.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                Editable s = numero.getEditableText();

                if(s.length() ==16 && numero.hasFocus()==false && !s.toString().contains("-") && fnumero) {
                    s.insert(4, "-");
                    s.insert(9, "-");
                    s.insert(14, "-");
                    System.out.println("numero.hasfacus"+numero.hasFocus() + "   hasfocus"+hasFocus);

                    fnumero=false;

                }
                else if(s.toString().length()==19&&numero.hasFocus()==true){
                    Editable ab = new SpannableStringBuilder(s.toString().replace("-", ""));
                    numero.setText(s.replace(0, s.length(), ab));
                    fnumero=true;


                }
            }
        });



        guardarTarjeta.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ingresar_cvc:

                break;
            case R.id.ingresar_mmyy:

                break;
            case R.id.ingresar_numero:

                break;
            case R.id.btn_guardar_tarjeta:
                goToTicket();

                break;
            default:
                break;



        }

    }

    private void goToTicket() {

        String barcode = b.getString("Barcode");

        Intent intent = new Intent(IngresarTarjeta.this,Ticket.class);
        intent.putExtra("Barcode",barcode);
        intent.putExtra("NumTarjeta",String.valueOf( numero.getText()));
        intent.putExtra("FechaVencimiento",String.valueOf(vencimiento.getText()));
        intent.putExtra("Monto",String.valueOf(monto.getText()));
        intent.putExtra("Horario",String.valueOf(horario.getText()));
        startActivity(intent);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override

    public void afterTextChanged(Editable s) {
        // evita que el usuario ingrese el simbolo -
        if(s==numero.getEditableText()){
            if (s.length() > 0 ) {
                final char c = s.charAt(s.length() - 1);
                if (minus == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            if(s.length()==16 && !s.toString().contains("-")&& fnumero == true){

                vencimiento.requestFocus();
            }


        }
        else if(s==vencimiento.getEditableText())
        {

            if (s.length() > 0 ) {
                final char c = s.charAt(s.length() - 1);
                if (barra == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            if (s.length() > 0 && (s.length() % 3) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(barra)).length <= 3) {
                        s.insert(s.length() - 1, String.valueOf(barra));
                }
            }
            if(s.length()>1&&s.length()<4){
                int n = 0;
                try{
                    n = Integer.parseInt(s.toString().substring(0,2));
                    }
                    catch (Exception e)
                    {
                        s.replace(0,s.length(),"");
                    }

                if(n>12){

                    s.replace(0,2,"12");
                }
            }
            if(s.length()>4){
                vencimiento.clearFocus();
                cvc.requestFocus();
            }

        }

    }

    @Override
    public boolean onKey(View view, int i, KeyEvent e) {
        switch (view.getId()){

            case R.id.ingresar_numero:
                if (e.getAction()==KeyEvent.ACTION_DOWN)
                {
                    if ((i == KeyEvent.KEYCODE_DPAD_CENTER) || (i == KeyEvent.KEYCODE_ENTER)){
                        System.out.println("preciono enter en numero");
                        vencimiento.requestFocus();
                        return true;
                    }

                }
                return false;
            case R.id.ingresar_mmyy:
                if (e.getAction()==KeyEvent.ACTION_DOWN)
                {
                    if ((i == KeyEvent.KEYCODE_DPAD_CENTER) || (i == KeyEvent.KEYCODE_ENTER)){
                        System.out.println("preciono enter en numero");
                        cvc.requestFocus();
                        return true;
                    }

                }
                return false;

            case R.id.ingresar_cvc:
                if (e.getAction()==KeyEvent.ACTION_DOWN)
                {
                    if ((i == KeyEvent.KEYCODE_DPAD_CENTER) || (i == KeyEvent.KEYCODE_ENTER)){
                        System.out.println("preciono enter en numero");
                        goToTicket();
                        return true;
                    }

                }
                return false;
        }
        return false;
    }

}
